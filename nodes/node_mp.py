#!/usr/bin/env python3
import rospy

from front_fishey_2d_body_pose_detector.front_fisheye_2d_body_pose_detector_mp_ros_node import FrontFisheye2DBodyPoseDetectorMp


if __name__ == '__main__':
    # Init node
    rospy.init_node('front_fisheye_2d_body_pose_detection_mp', log_level=rospy.DEBUG, anonymous=True)
    node = FrontFisheye2DBodyPoseDetectorMp()
    # node.run()
    rospy.spin()
