#!/usr/bin/env python3
import rospy

from front_fisheye_2d_body_pose_detector.front_fisheye_2d_body_pose_detector_op_ros_node import FrontFisheye2DBodyPoseDetectorOp


if __name__ == '__main__':
    # Init node
    rospy.init_node('front_fisheye_2d_body_pose_detection_op', log_level=rospy.DEBUG, anonymous=True)
    node = FrontFisheye2DBodyPoseDetectorOp()
    # node.run()
    rospy.spin()
    
