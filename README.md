# front_fisheye_2d_body_pose_detector_op

## topics used
Node [/front_fisheye_2d_body_pose_detector_op]
Publications: 
 * /rosout [rosgraph_msgs/Log]
 * /tracked_pose_2d/frame [spring_msgs/Frame]
 * /tracked_pose_2d/image_raw/compressed [sensor_msgs/CompressedImage]

Subscriptions: 
 * /frame [spring_msgs/Frame]
 * /rosOpenpose/camera/image [sensor_msgs/Image]
 * /tracker/tracker_output [std_msgs/String]

Services: 
 * /front_fisheye_2d_body_pose_detector_op/get_loggers
 * /front_fisheye_2d_body_pose_detector_op/set_logger_level

