#!/usr/bin/env python3
import numpy as np
import tf

def constraint_angle(angle, min_value=-np.pi, max_value=np.pi):

    length = max_value - min_value

    # if angle > max_value:
    #     diff = angle - max_value
    #     new_angle = min_value + (diff % length)
    # elif angle < min_value:
    #     diff = min_value - angle
    #     new_angle = max_value - (diff % length)
    # else:
    #     new_angle = angle
    new_angle = np.where(angle > max_value, min_value + ((angle - max_value) % length), angle)
    new_angle = np.where(angle < min_value, max_value - ((min_value - angle) % length), new_angle)
    return new_angle


def get_color(idx):
    idx = idx * 3
    color = ((37 * idx) % 255, (17 * idx) % 255, (29 * idx) % 255)

    return color