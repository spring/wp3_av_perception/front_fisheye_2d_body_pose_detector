#!/usr/bin/env python3
import rospy
import numpy as np
import cv2
from scipy.optimize import linear_sum_assignment
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from spring_msgs.msg import Frame
from hri_msgs.msg import Skeleton2D, NormalizedPointOfInterest2D
from std_msgs.msg import String
from sensor_msgs.msg import Image, CompressedImage
from front_fisheye_2d_body_pose_detector.utils import get_color
from cv_bridge import CvBridge


class FrontFisheye2DBodyPoseDetectorOp:
    def __init__(self):
        self.tracking_topic = rospy.get_param('~tracking_topic', '/tracker/tracker_output')
        self.frame_topic = rospy.get_param('~frame_topic', '/frame')
        self.image_pose_topic = rospy.get_param('~image_pose_topic', '/rosOpenpose/camera/image')
        self.diag_timer_rate = rospy.get_param('~diag_timer_rate', 10)

        self.track_data = None
        self.frame_data = None
        self.image_pose_data = None

        self.targets = []

        self.nb_joints = 25

        self.bridge = CvBridge()
        self.line_thickness = 4

        self._check_all_sensors_ready()

        self._publishers = []
        self._subscribers = []
        self._timers = []

        self.fr_hri_pub = dict()
        self._publishers.append(self.fr_hri_pub)
        self._subscribers.append(rospy.Subscriber(self.frame_topic, Frame, callback=self._frame_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.image_pose_topic, Image, callback=self._image_pose_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.tracking_topic, String, callback=self._tracking_callback, queue_size=1))

        self._tracked_pose_2d_pub  = rospy.Publisher('/tracked_pose_2d/frame', Frame, queue_size=10)
        self._publishers.append(self._tracked_pose_2d_pub)
        self._tracked_pose_2d_img_pub  = rospy.Publisher('/tracked_pose_2d/image_raw/compressed', CompressedImage, queue_size=10)
        self._publishers.append(self._tracked_pose_2d_img_pub)
        self._diagnostics_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=10)
        self._publishers.append(self._diagnostics_pub)

        self._timers.append(rospy.Timer(rospy.Duration(1/self.diag_timer_rate), callback=self._diagnostics_callback))

        rospy.on_shutdown(self.shutdown)        

        rospy.loginfo("front_fisheye_2d_body_pose_detection_op Initialization Ended")
  
  
    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        self._check_frame_ready()
        self._check_tracking_ready()
        self._check_image_pose_ready()
        rospy.logdebug("ALL SENSORS READY")

    
    def _check_tracking_ready(self):
        self.track_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.tracking_topic))
        while self.track_data is None and not rospy.is_shutdown():
            try:
                self.track_data = rospy.wait_for_message(self.tracking_topic, String, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.tracking_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for tracking results".format(self.tracking_topic))
        return self.track_data


    def _check_frame_ready(self):
        self.frame_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.frame_topic))
        while self.frame_data is None and not rospy.is_shutdown():
            try:
                self.frame_data = rospy.wait_for_message(self.frame_topic, Frame, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.frame_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for getting 2D openpose frames".format(self.frame_topic))
        return self.frame_data
    

    def _check_image_pose_ready(self):
        self.image_pose_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.image_pose_topic))
        while self.image_pose_data is None and not rospy.is_shutdown():
            try:
                self.image_pose_data = rospy.wait_for_message(self.image_pose_topic, Image, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.image_pose_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for getting 2D openpose image result".format(self.image_pose_topic))
        return self.image_pose_data


    def _frame_callback(self, data):
        self.frame_data = data
        self.img_height = self.image_pose_data.height
        self.img_width = self.image_pose_data.width
        h_ids = []

        if self.track_data is not None:
            splited_msg=self.track_data.data.split(',')
            delta_time = (self.frame_data.header.stamp).to_sec() - float(splited_msg[0])
            rospy.logdebug('Time latency between _frame_callback and (-) _tracking_callback : {}'.format(delta_time))

            self.targets = []
            ids_ordered_list = []
            fr = Frame()
            fr.header = self.frame_data.header

            for i in range(2, len(splited_msg)):
                target = splited_msg[i].split()
                x, y, width, height = int(float(target[1])), int(float(target[2])), int(float(target[3])), int(float(target[4]))
                if x > 300 and x < 900 : 
                    # Condition to consider only the bodies which are in between certain boundaries
                    # Above these conditions, the distorion of the fish eye makes the projection wrong 
                    self.targets.append(splited_msg[i].split())

            num_persons_tracked = len(self.targets)
            num_persons_pose_estimated = len(self.frame_data.persons)

            if num_persons_pose_estimated > 0 and num_persons_tracked > 0:
                joints_data = np.zeros((num_persons_pose_estimated, self.nb_joints, 2))
                for person_idx, person in enumerate(self.frame_data.persons):
                    for body_part_idx, body_part in enumerate(person.bodyParts):
                        joints_data[person_idx, body_part_idx, :] = [body_part.pixel.x, body_part.pixel.y]
                
                ids_ordered_list, idx_list = self.pose_tracking_matching(joints_data, num_persons_tracked)

                if len(ids_ordered_list) == num_persons_tracked and max(idx_list) < num_persons_pose_estimated:
                    fr.persons = np.array(self.frame_data.persons)[idx_list]
                    # if delta_time < 0 : 
                        # fr.header.stamp = rospy.Time.from_sec(float(splited_msg[0]))
                    fr.ids = ids_ordered_list
                    for idx, h_id in enumerate(ids_ordered_list):
                        if idx >= len(idx_list):
                            break

                        topic_name = "b{0:0=4d}".format(h_id)
                        if topic_name not in self.fr_hri_pub.keys():
                            self.fr_hri_pub[topic_name] = rospy.Publisher('/humans/bodies/' + topic_name + '/skeleton2d', Skeleton2D, queue_size=10)
                        fr_hri = Skeleton2D()
                        fr_hri.header.stamp = self.frame_data.header.stamp
                        fr_hri.header.frame_id = self.frame_data.header.frame_id
                        #fr_hri.header.data_source = self.frame_topic
                        fr_hri.skeleton = []
                        for joint_id in range(fr_hri.RIGHT_EAR + 1):
                            skeleton = NormalizedPointOfInterest2D()
                            if joint_id <= fr_hri.LEFT_WRIST:  # hri : COCO format, openpose : BODY_25 format
                                skeleton.x = fr.persons[idx].bodyParts[joint_id].pixel.x/self.img_width
                                skeleton.y = fr.persons[idx].bodyParts[joint_id].pixel.y/self.img_height
                                skeleton.c = fr.persons[idx].bodyParts[joint_id].score
                            else:
                                skeleton.x = fr.persons[idx].bodyParts[joint_id + 1].pixel.x/self.img_width
                                skeleton.y = fr.persons[idx].bodyParts[joint_id + 1].pixel.y/self.img_height
                                skeleton.c = fr.persons[idx].bodyParts[joint_id + 1 ].score
                            fr_hri.skeleton.append(skeleton)
                        self.fr_hri_pub[topic_name].publish(fr_hri)
                        h_ids.append(topic_name)
            self._tracked_pose_2d_pub.publish(fr)

        for id in list(self.fr_hri_pub.keys()):
            if id not in h_ids:
                self.fr_hri_pub[id].unregister()
                del self.fr_hri_pub[id]

    
    def _tracking_callback(self, data):
        self.track_data = data
        # assuming that the 2D tracker with be faster than the pose estimation through Openpose


    def _image_pose_callback(self, data):
        self.image_pose_data = data
        
        if self.track_data is not None:
            image = self.bridge.imgmsg_to_cv2(self.image_pose_data, "rgb8")

            for target in list(self.targets):
                id = int(target[0])
                x, y, width, height = int(float(target[1])), int(float(target[2])), int(float(target[3])), int(float(target[4]))
                color = get_color(id)
                cv2.rectangle(image, (x, y), (x + width, y + height), color=color, thickness=self.line_thickness)
                cv2.putText(image, 'ID : ' + "b{0:0=4d}".format(id), org=(x, y - 20), fontFace=cv2.FONT_HERSHEY_SIMPLEX,  fontScale=0.8, color=color, thickness=self.line_thickness, lineType=cv2.LINE_AA)
            
            #### Create CompressedIamge ####
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            img_msg = CompressedImage()
            img_msg.header = self.image_pose_data.header
            img_msg.format = "jpeg"
            img_msg.data = np.array(cv2.imencode('.jpg', image)[1]).tostring()
            # img_msg = self.bridge.cv2_to_imgmsg(image, "bgr8")
            # img_msg.header = self.image_pose_data.header
            self._tracked_pose_2d_img_pub.publish(img_msg)

    # associates openpose skeletons detections (N joints_data) with 2d tracker bounding boxes (M self.targets) :
    # fill a matrix (NxM) with each cell containing the Jmax - (number of joints from detection N inside each of the M bounding box)
    # then find the best associations with the hungarian algorithm
    def pose_tracking_matching(self, joints_data, num_persons_tracked):
        pose_target_idx = np.zeros((num_persons_tracked, joints_data.shape[0])) + self.nb_joints
        ids = np.zeros(num_persons_tracked) - 1
        
        for person_idx, person_data in enumerate(joints_data):
            for joint_data in person_data:
                for target_idx, target in enumerate(list(self.targets)):
                    ids[target_idx] = int(target[0])
                    x, y, width, height = int(float(target[1])), int(float(target[2])), int(float(target[3])), int(float(target[4]))
                    if x <= joint_data[0] <= x + width and y <= joint_data[1] <= y + height:
                        pose_target_idx[target_idx, person_idx] -= 1
        row_ind, col_ind = linear_sum_assignment(pose_target_idx)
        return ids.astype(np.int16), col_ind


    def _diagnostics_callback(self, event):
        self.publish_diagnostics()


    def publish_diagnostics(self):
        diagnostic_msg = DiagnosticArray()
        diagnostic_msg.status = []
        diagnostic_msg.header.stamp = rospy.Time.now()
        status = DiagnosticStatus()
        status.name = 'Functionality: AV Perception: Front Fisheye 2D Body Pose Detector with Openpose'
        status.values.append(KeyValue(key='Number of tracks from the Tracker', value='{}'.format(len(self.targets))))
        status.values.append(KeyValue(key='Number of detected skeletons with Openpose', value='{}'.format(len(self.frame_data.persons))))
        status.values.append(KeyValue(key='Number of tracks-skeletons matchs', value='{}'.format(len(self.fr_hri_pub))))

        # by default
        status.level = DiagnosticStatus.OK
        status.message = ''
        if not self.targets:
            status.level = DiagnosticStatus.OK
            status.message = 'No human tracked in the front fisheye 2D plane as input'
        if not self.frame_data.persons:
            status.level = DiagnosticStatus.OK
            status.message = 'No skeleton detected with Openpose in the front fisheye 2D plane as input'
        if not self.image_pose_data:
            status.level = DiagnosticStatus.WARN
            status.message = 'No image with skeleton detected displayed from Openpose in the front fisheye 2D plane as input'
        if self.targets and self.frame_data.persons and not self.fr_hri_pub:
            status.level = DiagnosticStatus.WARN
            status.message = 'Was not able to match any detected skeletons from Opensepose with any tracks from the tracker in the front fisheye 2D plane'

        diagnostic_msg.status.append(status)
        self._diagnostics_pub.publish(diagnostic_msg)


    def shutdown(self):
        rospy.loginfo("Stopping the front_fisheye_2d_body_pose_detection_op node")
        self.close()
        rospy.loginfo("Killing the front_fisheye_2d_body_pose_detection_op node")


    def close(self):
        if self._subscribers:
            for subscriber in self._subscribers:
                subscriber.unregister()
        if self._publishers:
            for publisher in self._publishers:
                if isinstance(publisher, dict):
                    for pub in publisher.values():
                        pub.unregister()
                else:
                    publisher.unregister()
        if self._timers:
            for timer in self._timers:
                timer.shutdown()
