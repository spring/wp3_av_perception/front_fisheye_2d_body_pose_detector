#!/usr/bin/env python3
import rospy
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
import mediapipe as mp
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose

from spring_msgs.msg import Frame, Person, BodyPart
from sensor_msgs.msg import Image, CompressedImage
from std_msgs.msg import String
from front_fisheye_2d_body_pose_detector.utils import constraint_angle, get_color


class FrontFisheye2DBodyPoseDetectorMp:
    def __init__(self):
        self.tracking_topic = rospy.get_param('~tracking_topic', '/tracker/tracker_output')
        self.image_topic = rospy.get_param('~image_topic', '/front_fisheye_basestation/image_raw')

        self.img_data = None
        self.track_data = None

        self.current_time = None
        self.bridge = CvBridge()
        self.line_thickness = 4

        self._publishers = []
        self._subscribers = []

        self._check_all_sensors_ready()

        self._subscribers.append(rospy.Subscriber(self.image_topic, Image, callback=self._image_callback, queue_size=1))
        self._subscribers.append(rospy.Subscriber(self.tracking_topic, String, callback=self._tracking_callback, queue_size=1))

        self._tracked_pose_2d_pub  = rospy.Publisher('/tracked_pose_2d/frame', Frame, queue_size=10)
        self._publishers.append(self._tracked_pose_2d_pub)
        self._tracked_pose_2d_img_pub = rospy.Publisher('/tracked_pose_2d/image_raw/compressed', CompressedImage, queue_size=10)
        self._publishers.append(self._tracked_pose_2d_img_pub)

        rospy.on_shutdown(self.shutdown)        

        rospy.loginfo("front_fisheye_2d_body_pose_detection_mp Initialization Ended")


    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        self._check_image_ready()
        self._check_tracking_ready()
        rospy.logdebug("ALL SENSORS READY")


    def _check_image_ready(self):
        self.img_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.image_topic))
        while self.img_data is None and not rospy.is_shutdown():
            try:
                self.img_data = rospy.wait_for_message(self.image_topic, Image, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.image_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for image".format(self.image_topic))
        return self.img_data


    def _check_tracking_ready(self):
        self.track_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.tracking_topic))
        while self.track_data is None and not rospy.is_shutdown():
            try:
                self.track_data = rospy.wait_for_message(self.tracking_topic, String, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.tracking_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for tracking results".format(self.tracking_topic))
        return self.track_data


    def _image_callback(self, data):
        self.img_data = data

    def _tracking_callback(self, data):
        self.track_data = data
        self._targets = []

        if self.img_data is not None:


            splited_msg=data.data.split(',')
            self.current_time = splited_msg[0]
            for i in range(2, len(splited_msg)):
                self._targets.append(splited_msg[i].split())
            num_persons = len(self._targets)

            image = self.bridge.imgmsg_to_cv2(self.img_data, "rgb8")

            fr = Frame()
            fr.header = self.img_data.header
            # Handle body points
            fr.persons = [Person() for _ in range(num_persons)]
            fr.ids = [int(target[0]) for target in self._targets]

            if num_persons != 0:

                with mp_pose.Pose(static_image_mode=True,
                                  model_complexity=1,
                                  min_detection_confidence=0.2) as pose:

                    for i, target in enumerate(self._targets):
                        id = int(target[0])
                        x, y, width, height = int(float(target[1])), int(float(target[2])), int(float(target[3])), int(float(target[4]))
                        roi = image[y:y + height, x:x + width]
                        color = get_color(id)
                        cv2.rectangle(image, (x, y), (x + width, y + height), color=color, thickness=self.line_thickness)
                        cv2.putText(image, 'ID :' + str(id), org=(x, y - 20), fontFace=cv2.FONT_HERSHEY_SIMPLEX,  fontScale=0.8, color=color, thickness=self.line_thickness, lineType=cv2.LINE_AA)

                        try:
                            results = pose.process(roi)
                            pose_landmarks = results.pose_landmarks
                        except Exception as e:
                            rospy.logwarn("Could not get pose landmarks: {}".format(e))
                            continue


                        keypoints = []
                        if pose_landmarks is not None:
                            for data_point in results.pose_landmarks.landmark:
                                keypoints.append([data_point.x, data_point.y, data_point.z, data_point.visibility])
                            body_part_count = len(keypoints)

                            # Draw the pose annotation on the image.
                            # image.flags.writeable = True
                            # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                            mp_drawing.draw_landmarks(
                            roi,
                            results.pose_landmarks,
                            mp_pose.POSE_CONNECTIONS,
                            landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())
                            # Flip the image horizontally for a selfie-view display.
                            # cv2.imshow('MediaPipe Pose', cv2.flip(image, 1))
                            # if cv2.waitKey(5) & 0xFF == 27:
                            #   break
                            fr.persons[i].bodyParts = [BodyPart() for _ in range(body_part_count)]
                            fr.ids[i] = id
                            # no leftHandParts nor rightHandParts
                            # Process the body
                            for bp_idx, bp in enumerate(keypoints):
                                u, v, s = bp[0]*width + x, bp[1]*height + y, bp[-1]
                                bp_x, bp_y, bp_z = bp[0], bp[1], bp[2]  # warning: in the image plane, see medip documentation https://google.github.io/mediapipe/solutions/pose
                                arr = fr.persons[i].bodyParts[bp_idx]
                                arr.pixel.x = u
                                arr.pixel.y = v
                                arr.score = s
                                arr.point.x = bp_x
                                arr.point.y = bp_y
                                arr.point.z = bp_z

            #### Create CompressedIamge ####
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            img_msg = CompressedImage()
            img_msg.header = self.img_data.header
            img_msg.format = "jpeg"
            img_msg.data = np.array(cv2.imencode('.jpg', image)[1]).tostring()
            # img_msg = self.bridge.cv2_to_imgmsg(image, "rgb8")
            # img_msg.header = self.img_data.header
            self._tracked_pose_2d_img_pub.publish(img_msg)
            self._tracked_pose_2d_pub.publish(fr)


    def shutdown(self):
        rospy.loginfo("Stopping the front_fisheye_2d_body_pose_detection_op node")
        self.close()
        rospy.loginfo("Killing the front_fisheye_2d_body_pose_detection_op node")


    def close(self):
        if self._subscribers:
            for subscriber in self._subscribers:
                subscriber.unregister()
        if self._publishers:
            for publisher in self._publishers:
                publisher.unregister()
